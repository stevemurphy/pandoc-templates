#!/bin/bash
pandoc -r markdown -w html -s -S --bibliography=$(HOME)/Library/texmf/bibtex/bib/eddbib.bib --csl=harvard-swinburne-university-of-technology.csl