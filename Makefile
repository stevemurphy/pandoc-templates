## Started from work of Kieran Healy
## From https://github.com/kjhealy/pandoc-templates
## Last updated: 18 February 2021
## Last update accommodates changes to citeproc (now part of pandoc) and need for resource file paths

## Put this Makefile in your project directory---i.e., the directory
## containing the paper you are writing. Assuming you are using the
## rest of the toolchain here, you can use it to create .html, .tex, .docx
## and .pdf output files (complete with bibliography, if present) from
## your markdown file.
##
##  Change the paths/options at the top of the file as needed.
##
##  Using `make all` will generate html, tex, docx and pdf
## 	output files from all files in the folder with the designated
##	markdown extension. The default is `.md` but you can change this.
##
##  You can specify an output format with `make tex`, `make pdf`,
## `make docx` or `make html`.
##
##  Added handling of mmd files (using multimarkdown processor)
##
##  You can specify a single file e.g `make file.tex`.
##  You can change defaults when calling e.g `make file.tex CSL=ieee`
##  You can add options when calling e.g `make file.tex OPTIONS=--toc`
##
##  You can quickly typeset an existing .tex file, but note there is
##  no error checking for this.
##
##  Doing `make cleantargets` will remove all the .docx, .html, and .pdf files
## 	in your working directory, as well as most files which might be
## 	generated from standard LaTeX/BibTeX typesetting.
## 	All of these should be able to be recreated easily, but make sure
## 	you do not have files in these formats that you want to keep!

## Any of these variables could be changed when running make e.g:
## `make pdf TMPL=basic CSL=ieee`

## Markdown extension (e.g. md, markdown, mdown).
MEXT=	md
MMEXT=	mmd

## All markdown files in the working directory
SRC=	$(wildcard *.$(MEXT))
MMDSRC=	$(wildcard *.$(MMEXT))

## Location of Pandoc support files.
PREFIX=	$(HOME)/.pandoc

# Default resource file path includes current directory and csl files repo
RESPATH=.:$(HOME)/.csl

## Location of your working bibliography file
BIB=	$(HOME)/Library/texmf/bibtex/bib/eddbib.bib

## CSL stylesheet (located in the csl folder of the PREFIX directory).
CSL=	harvard-swinburne-university-of-technology

## LaTeX template (default is default!)
TMPL=	default

## When making a standalone tex file, use biblatex by default
BIBP=	--biblatex


## Set the reference file for docx targets
## either set by user with RDOC (ref file assumed in templates dir) or
## if RDOC empty don't set (defaults to reference.docx in user data dir)
ifeq ($(RDOC),)
  refdocx :=
else
  refdocx :=	--reference-doc=$(PREFIX)/templates/$(RDOC).docx
endif


PDFS=	$(SRC:%.md=%.pdf)
HTML=	$(SRC:%.md=%.html)
TEX=	$(SRC:%.md=%.tex)
DOCX=	$(SRC:%.md=%.docx)

MMDT=	$(MMDSRC:%.mmd=%.tex)


.PHONY: help defaults
.PHONY: all
.PHONY: clean cleantargets
.PHONY: cleanfor
.PHONY: hclean pclean dclean tclean lclean

help::
	@echo
	@echo "-----------------------------------------------------"
	@echo
	@echo "Put this Makefile in your project directory---i.e."
	@echo "the directory containing the document you are writing."
	@echo
	@echo "You can use it to create .html, .tex, .docx and .pdf output files"
	@echo "(complete with bibliography, if present) from your markdown file."
	@echo
	@echo "Using make all will generate html, tex, docx and pdf output files"
	@echo "from all files in the folder with the defined markdown extension."
	@echo
	@echo "You can specify any single output format:"
	@echo "    - make tex"
	@echo "    - make pdf"
	@echo "    - make html"
	@echo "    - make docx"
	@echo
	@echo "These will generate the targets for all files in the folder with the"
	@echo "defined markdown extension. Any existing files of that format will be"
	@echo "removed first, so be sure you there are no files that need to be kept."
	@echo
	@echo "You can also specify a single target:"
	@echo "    - make file.tex"
	@echo "The source will be the markdown file with the matching name."
	@echo
	@echo "You can create .tex files for all mmd source files:"
	@echo "    - make mmd2tex"
	@echo
	@echo "Defaults can be changed on the command line:"
	@echo "    - make file.pdf TMPL=basic CSL=ieee"
	@echo "    - make file.docx RDOC=Refdoc    (don't include .docx extension)"
	@echo
	@echo "Pandoc options can be added explicitly:"
	@echo "    - make file.pdf OPTIONS=--toc"
	@echo "    - make file.tex OPTIONS='--toc --number-sections'"
	@echo
	@echo "Use 'make defaults' to see the common defaults."
	@echo
	@echo "Multimarkdown files will be handled with the mmd tools (and latex), not pandoc."
	@echo
	@echo "You can typeset an existing .tex file:"
	@echo "    - make file.pdf"
	@echo "Note there is no error checking for this."
	@echo
	@echo "    - make cleantargets - will remove all the .docx, .html, and .pdf"
	@echo "      files in your working directory."
	@echo
	@echo "    - make pclean - will remove all the .pdf files in your working directory."
	@echo "    - make dclean - will remove all the .docx files in your working directory."
	@echo "    - make tclean - will remove all the .tex files in your working directory."
	@echo "    - make hclean - will remove all the .html files in your working directory."
	@echo
	@echo "    - make lclean - will remove most files which would be"
	@echo "      generated by standard LaTeX/BibTeX typesetting."
	@echo
	@echo "    - make file.cleanfor - will remove file.pdf, file.tex, etc"
	@echo
	@echo "      All of these should be able to be recreated easily, but make sure"
	@echo "      you do not have files in these formats that you want to keep!"
	@echo


defaults:
	@echo
	@echo "-----------------------------------------------------"
	@echo
	@echo "Some useful defaults which can be changed when calling, e.g.:"
	@echo "    - make file.pdf TMPL=basic CSL=ieee"
	@echo
	@echo "MEXT=		$(MEXT)"
	@echo "MMEXT=		$(MMEXT)"
	@echo "BIB=		$(BIB)"
	@echo "CSL=		$(CSL)"
	@echo "TMPL=		$(TMPL)"
	@echo "BIBP=		$(BIBP)"
	@echo "RDOC=		[a ref docx in the templates dir -- don't include .docx]"
	@echo "OPTIONS=	[any standard pandoc command line options e.g. --toc]"
	@echo
	@echo "Note that:"
	@echo
	@echo "MEXT  sets the extension for markdown files to be converted"
	@echo "MMEXT sets the extension for multimarkdown files to be converted"
	@echo "BIBP  is only used for making .tex files via pandoc"
	@echo "      (use BIBP= for standard pandoc-citeproc bib processing)"
	@echo "RDOC  is only used for making .docx files"
	@echo "TMPL  is used for both .tex and .pdf files"
	@echo "All the rest apply for all formats"
	@echo

all:		$(PDFS) $(HTML) $(TEX) $(DOCX) mmd2tex

pdf:		pclean $(PDFS)
html:		hclean $(HTML)
tex:		$(TEX)
docx:		dclean $(DOCX)

mmd2tex:	$(MMDT)


%.html:	%.$(MEXT)
	pandoc -r markdown+simple_tables+table_captions+yaml_metadata_block+smart -w html -s --template=$(PREFIX)/templates/html.template --css=$(PREFIX)/css/kultiad-serif.css --citeproc --csl=$(CSL).csl --bibliography=$(BIB) $(OPTIONS) -o $@ $<

## Alternate (double-colon) rules for generating tex file from a pandoc markdown or multimarkdown file (i.e. different dependencies)
%.tex::	%.$(MEXT)
	pandoc -r markdown+simple_tables+table_captions+yaml_metadata_block -w latex -s --latex-engine=pdflatex --template=$(TMPL) --csl=$(CSL).csl --bibliography=$(BIB) $(BIBP) $(OPTIONS) -o $@ $<

%.tex:: %.$(MMEXT)
	multimarkdown -t latex $< > $@

## Alternate (double-colon) rules for generating pdf depending on whether md or mmd source, or just an existing tex file
%.pdf::	%.$(MEXT)
	pandoc -r markdown+simple_tables+table_captions+yaml_metadata_block+smart -s --latex-engine=pdflatex --template=$(TMPL) --citeproc --csl=$(CSL).csl --bibliography=$(BIB) $(OPTIONS) -o $@ $<

%.pdf:: %.$(MMEXT)
	if [ ! -f $*.tex ]; then multimarkdown -t latex $< > $*.tex ; fi;
	pdflatex $*.tex; bibtex $*; pdflatex $*.tex; pdflatex $*.tex
	@-rm -f *.lox *.lof *.lot *.toc *.upa *.upb *.aux *.log *.out *.bbl *.blg *.bcf *.run.xml *.fdb_latexmk *-blx.bib *.synctex.gz .log

%.pdf:: %.tex
	pdflatex $*.tex; bibtex $*; pdflatex $*.tex; pdflatex $*.tex

%.docx:	%.$(MEXT)
	pandoc -r markdown+simple_tables+table_captions+yaml_metadata_block+smart -s --csl=$(CSL).csl --bibliography=$(BIB) --citeproc $(refdocx) --resource-path=$(RESPATH) $(OPTIONS) -o $@ $<

## A slightly clumsy way to clean just the target files for a particular source
## Run `make file.cleanfor` to remove file.html, file.pdf, etc
%.cleanfor:
	@-rm -f $*.html $*.pdf $*.docx $*.tex

clean:
	@-rm -f *.html *.pdf *.docx

cleantargets: pclean dclean hclean

hclean:
	@-rm -f *.html

pclean:
	@-rm -f *.pdf

dclean:
	@-rm -f *.docx

tclean:
	@-rm -f *.tex

lclean:
	@-rm -f *.lox *.lof *.lot *.toc *.upa *.upb
	@-rm -f *.aux *.log *.out *.bbl *.blg *.bcf .log
	@-rm -f *.run.xml *.fdb_latexmk *-blx.bib *.synctex.gz
