---
title: 'Pandoc Markdown Article Starter'
subtitle: 'Based on the K Healy template'
author:
- name: Stephen Murphy
  affiliation: University of Technology, Sydney
  email: stephen.j.murphy@alumni.uts.edu.au
- name: Kieran Healy
  affiliation: Duke University
  email: kjhealy@soc.duke.edu
date: 13 June 2014
abstract: |
  This is the abstract.

  YAML escaping rules must be followed in the header. Thus, for example, if a title contains a colon, it must be quoted. The pipe character (|) can be used to begin an indented block that will be interpreted literally, without need for escaping. This form is necessary when the field contains blank lines, such as this abstract of multiple paragraphs.

  If there was only one paragraph, it could follow directly on from the abstract YAML field name without the pipe, just a space (as in the `article-options.md` template).
...


# Introduction

Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua [@Vygotsky1978]. Notice that Pandoc citation there, and here, where Bourdieu [-@Bourdieu1977] is not at all relevant. 

# Theory

Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enimad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.

Ut enimad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

# Conclusion

Right now, your options for formatting bibliographies using Pandoc are to offload the work to biblatex (or natbib), or have pandoc handle everything via `citeproc` and CSL files. The latter way is simpler and cleaner, especially if we want to preserve the ability to easily generate both HTML and LaTeX/PDF outputs. You have to do two things. First, explicitly specify the "References" header. Second, pandoc does not (yet) support some standard layout options for bibliography entries---it will treat each entry like a regular paragraph, when we want the first lines of each bibliography entry to have no indentation, with subsequent lines (if any) to hang in from the margin. The LaTeX commands below the "References" header accomplish this. The LaTeX commands are ignored when HTML is produced, and do not show up in the output file.

# References
<!-- Delete this section if creating a tex doc rather than typesetting directly via pandoc -->
\setlength{\parindent}{-0.2in}
\setlength{\leftskip}{0.2in}
\setlength{\parskip}{8pt}
\vspace*{-0.2in}
\noindent