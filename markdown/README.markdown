README

Markdown and Multimarkdown template files for use with pandoc and LaTeX.

Created by Stephen Murphy on 2014-06-13

# Folder Contents

Article/writing starter templates primarily for dissertation and other academic/research writing. Intended for pandoc or multimarkdown conversion (either directly or via Makefile) to create finished documents.

The markdown files are for use with pandoc, and include various metadata/variable options that would be used for conversion to pdf, html, doc or tex files. The multimarkdown files would normally be converted to tex files using the standard utilities.

The multimarkdown files can also be used as a simple wrapper for an `exported.tex` include file, to create a complete LaTeX document ready for immediate compilation with minimal extra work.

A simple makefile is included that can be used as a starter for local customisation of a project.

Note that style files, BibTeX files, mmd headers, includes, etc. would normally be in the texmf tree.

This folder is part of a git repo including various pandoc support files.

# Usage

Simply choose the most appropriate starter template and then copy this to a working project directory. The simple makefile in this folder can also be copied to that directory, pointing to the master Makefile and allowing for easy customisation/overrides. That project directory would typically be under version control in its own repo.

# License

Original work here is copyright Stephen Murphy <mailto:stephen.j.murphy@alumni.uts.edu.au>, although there have been many influences over the years affecting the final result.

Some of the template files, the general method for using pandoc, and the main Makefile are based on the work of Kieran Healy: <https://github.com/kjhealy/pandoc-templates>.

These files have been developed by the author for personal use, and are not in any way authorised by University of Technology, Sydney (UTS).

Any and all of the author's rights are currently reserved.