---
title: Pandoc Annotated Bibliography Article
author:
- name: Stephen Murphy
  affiliation: University of Technology, Sydney
  email: stephen.j.murphy@alumni.uts.edu.au
date: 13 June 2014
abstract: A simple Pandoc markdown document to create an annotated bibliography. 
...

---
keywords:
  - annotated
  - bibliography
...

<!-- This only works by creating a tex document first, then typesetting that: the Pandoc filter does not work with the annotated bibliography. Use the annobib Pandoc template to create the tex doc. -->

<!-- Be sure to set the natbib option on the command line or via the makefile -->

<!-- References can be added here in the body as LaTeX nocites or via a file of nocites to be included with the -B option either on the command line or in the makefile. -->

# Note

This annotated bibliography is a work in progress. The references here are those for which an annotation has been written. This is not a complete bibliographic database dump.

\nocite{Bourdieu1977}
\nocite{Feynman1963}
