---
title: Pandoc Markdown Article Starter With Optional Goodness
subtitle: Includes some font options and a subtitle
author: Stephen Murphy
date: 13 June 2014
abstract: This is the abstract. It consists of a single paragraph. For more than one par, YAML escaping rules need to be followed. See the `article-YAML.md` template for an example.
...

---
classoption: draft
graphics: true
keywords:
  - draft
  - pandoc
biblio-style: smuts
biblio-title: References
fontfamily: sourcesanspro
fontoptions: default
sansbase: false
...
<!-- Note: some YAML options such as TOC might cause spurious output with current HTML template. For that case, do not include in md block and set on command line (e.g via OPTIONS in makefile) -->

# Introduction

A Pandoc markdown template, using multiple YAML metadata blocks and showing how extra compile options (Pandoc variables/metadata) can be set.

From the Pandoc User's Guide:

> Metadata will be taken from the fields of the YAML object and added to any existing document metadata. Metadata can contain lists and objects (nested arbitrarily), but all string scalars will be interpreted as markdown. Fields with names ending in an underscore will be ignored by pandoc. (They may be given a role by external processors.)

> A document may contain multiple metadata blocks. The metadata fields will be combined through a left-biased union: if two metadata blocks attempt to set the same field, the value from the first block will be taken.

_Note: some of the options above, such as the font options, may only be available with some pandoc latex templates, e.g. basictex.latex._

# Theory

Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua [@Vygotsky1978]. Notice that Pandoc citation there, and here, where Bourdieu [-@Bourdieu1977] is not at all relevant.

Ut enimad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

# Conclusion

Right now, your options for formatting bibliographies using Pandoc are to offload the work to biblatex (or natbib), or have pandoc handle everything via `citeproc` and CSL files. The latter way is simpler and cleaner, especially if we want to preserve the ability to easily generate both HTML and LaTeX/PDF outputs. You have to do two things. First, explicitly specify the "References" header. Second, pandoc does not (yet) support some standard layout options for bibliography entries---it will treat each entry like a regular paragraph, when we want the first lines of each bibliography entry to have no indentation, with subsequent lines (if any) to hang in from the margin. The LaTeX commands below the "References" header accomplish this. The LaTeX commands are ignored when HTML is produced, and do not show up in the output file.

# References
<!-- Delete this section if creating a tex doc rather than typesetting directly via pandoc -->
\setlength{\parindent}{-0.2in}
\setlength{\leftskip}{0.2in}
\setlength{\parskip}{8pt}
\vspace*{-0.2in}
\noindent