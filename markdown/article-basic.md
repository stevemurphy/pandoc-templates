% Pandoc Markdown Article Starter
% Stephen Murphy
% 13 June 2014

# Introduction

A basic markdown template, using a simple pandoc title block instead of a YAML metadata block.

The title block may contain just a title, a title and an author, or all three elements. If you want to include an author but no title, or a title and a date but no author, you need a blank line:

```
%
% Author

% My title
%
% June 15, 2006
```

See the Pandoc User's Guide for more, such as titles on multiple lines, or multiple authors. For more complex options it is probably better to use the YAML metadata block version. 

# Theory

Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua [@Vygotsky1978]. Notice that Pandoc citation there, and here, where Bourdieu [-@Bourdieu1977] is not at all relevant.

Ut enimad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

# Conclusion

Right now, your options for formatting bibliographies using Pandoc are to offload the work to biblatex (or natbib), or have pandoc handle everything via `citeproc` and CSL files. The latter way is simpler and cleaner, especially if we want to preserve the ability to easily generate both HTML and LaTeX/PDF outputs. You have to do two things. First, explicitly specify the "References" header. Second, pandoc does not (yet) support some standard layout options for bibliography entries---it will treat each entry like a regular paragraph, when we want the first lines of each bibliography entry to have no indentation, with subsequent lines (if any) to hang in from the margin. The LaTeX commands below the "References" header accomplish this. The LaTeX commands are ignored when HTML is produced, and do not show up in the output file.

# References
<!-- Delete this section if creating a tex doc rather than typesetting directly via pandoc -->
\setlength{\parindent}{-0.2in}
\setlength{\leftskip}{0.2in}
\setlength{\parskip}{8pt}
\vspace*{-0.2in}
\noindent