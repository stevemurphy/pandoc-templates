---
title: Pandoc Markdown Article Starter With Included References
author: Stephen Murphy
date: 13 June 2014
abstract: This is the abstract. It consists of a single paragraph. For more than one par, YAML escaping rules need to be followed. See the `article-YAML.md` template for an example.
...

---
references:
- id: fenner2012a
  title: One-click science marketing
  author:
  - family: Fenner
    given: Martin
  container-title: Nature Materials
  volume: 11
  URL: 'http://dx.doi.org/10.1038/nmat3283'
  DOI: 10.1038/nmat3283
  issue: 4
  publisher: Nature Publishing Group
  page: 261-263
  type: article-journal
  issued:
    year: 2012
    month: 3

- id: Bourdieu1977
  title: Outline of a theory of practice
  author:
  - family: Bourdieu
    given: Pierre
  publisher: Cambridge University Press
  publisher-place: Cambridge, UK
  type: book
  issued:
    year: 1977

- id: Ellis2000
  title: 'Autoethnography, personal narrative, reflexivity: Researcher as subject'
  author:
  - family: Ellis
    given: Carolyn
  - family: Bochner
    given: Arthur P.
  container-title: The handbook of qualitative research
  editor:
  - family: Denzin
    given: Norman K.
  - family: Lincoln
    given: Yvonna S.
  publisher: Sage Publications
  publisher-place: Thousand Oaks, CA
  page: 733-768
  type: chapter
  issued:
    year: 2000

- id: Kruger1999
  title: 'Unskilled and unaware of it: How difficulties in recognizing one''s own incompetence lead to inflated self-assessments'
  author:
  - family: Kruger
    given: Justin
  - family: Dunning
    given: David
  container-title: Journal of Personality and Social Psychology
  volume: 77
  issue: 6
  page: 1121-1134
  type: article-journal
  issued:
    year: 1999

- id: Lee2000
  title: 'Organisational knowledge, professional practice and the professional doctorate at work'
  author:
  - family: Lee
    given: Alison
  - family: Green
    given: Bill
  - family: Brennan
    given: Marie
  container-title: 'Research and knowledge at work: Perspectives, case-studies and innovative strategies'
  editor:
  - family: Garrick
    given: John
  - family: Rhodes
    given: Carl
  publisher: Routledge
  publisher-place: New York and London
  page: 117-136
  type: chapter
  issued:
    year: 2000

- id: Vygotsky1978
  title: Mind in society
  author:
  - family: Vygotsky
    given: Lev Semyonovich
  publisher: Harvard University Press
  type: book
  issued:
    year: 1978
...


# Introduction

A Pandoc markdown template, using multiple YAML metadata blocks and showing how **references** can be included in the file as YAML-encoded data. Note that this requires a lot of work by hand, and would have limited use. The YAML format is based on CSL JSON, but there is no conversion tool from other formats. Might occasionally be useful for something with only a few references where everything needs to be self-contained.

# Theory

Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua [@fenner2012a]. Notice that Pandoc citation there, which uses an example reference from the Pandoc User's Guide. Here I will refer to Bourdieu [-@Bourdieu1977] even though it is not at all relevant. I will also mention some other irrelevant sources, so that they are included in the bibliography [@Ellis2000; @Kruger1999; @Lee2000; @Vygotsky1978].

Ut enimad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

# Conclusion

Right now, your options for formatting bibliographies using Pandoc are to offload the work to biblatex (or natbib), or have pandoc handle everything via `citeproc` and CSL files. The latter way is simpler and cleaner, especially if we want to preserve the ability to easily generate both HTML and LaTeX/PDF outputs. You have to do two things. First, explicitly specify the "References" header. Second, pandoc does not (yet) support some standard layout options for bibliography entries---it will treat each entry like a regular paragraph, when we want the first lines of each bibliography entry to have no indentation, with subsequent lines (if any) to hang in from the margin. The LaTeX commands below the "References" header accomplish this. The LaTeX commands are ignored when HTML is produced, and do not show up in the output file.

# References

\setlength{\parindent}{-0.2in}
\setlength{\leftskip}{0.2in}
\setlength{\parskip}{8pt}
\vspace*{-0.2in}
\noindent

